<?php

/**
 * @file
 * This file is used to write the code to generate the login link in the module.
 */

namespace Drupal\one_time_login_link_admin\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Mail\MailManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines OneTimeLoginLinkController Class.
 *
 * @package Drupal\one_time_login_link_admin\Controller
 */
class OneTimeLoginLinkController extends ControllerBase {

  /**
   * Date time formatter service.
   *
   * @var Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Constructs the service objects.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formater
   *   The date formatter service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *  The mail manager service.
   */
  public function __construct(DateFormatter $date_formater, MailManagerInterface $mail_manager) {
    $this->dateFormatter = $date_formater;
    $this->mailManager = $mail_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('plugin.manager.mail'),
    );
  }

  /**
   * Generates a one-time login (password reset) link for the given user.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user for which to generate the login link.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   A redirect to the destination, if one was provided.
   */
  public function generateLoginLink(AccountInterface $user) {
    $timeout = $this->config('user.settings')->get('password_reset_timeout');
    // Adding "/login" to the Drupal-supplied one-time login link removes one
    // step from the process: instead of users having to click "Log in" on an
    // unnecessary intermediate screen, log them in directly.
    $url = user_pass_reset_url($user) . '/login';
    $mail = $user->getEmail();
    $this->messenger()->addMessage($this->t('One-time login link created for %mail:<br/> <code>%login</code>', [
      '%mail' => $mail,
      '%login' => $url,
    ]));

    $this->messenger()->addMessage($this->t("This link is valid for %hr, and will become invalid if the user's password is updated by anyone.", [
      '%hr' => $this->dateFormatter->formatInterval($timeout),
    ]));

    if ($destination = $this->getRedirectDestination()->get()) {
      return new RedirectResponse($destination);
    }
  }

  /**
   * Generates and send a one-time login (password reset) link for the selected user.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user for which to generate the login link.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   A redirect to the destination, if one was provided.
   */
  public function emailLoginLink(AccountInterface $user) {
    $timeout = $this->config('user.settings')->get('password_reset_timeout');
    $formatedTimeout = $this->dateFormatter->formatInterval($timeout);
    // Adding "/login" to the Drupal-supplied one-time login link removes one
    // step from the process: instead of users having to click "Log in" on an
    // unnecessary intermediate screen, log them in directly.
    $url = user_pass_reset_url($user) . '/login';
    $mail = $user->getEmail();
    $label = $this->config('system.site')->get('name');

    $params['title'] = $label;
    $params['body'] = [$this->t("Your one-time login link is created for <b>%site:</b> by admin.<br>Please access from here: %login.<br><br>This link is valid for %hr, and will become invalid if the user's password is updated by anyone.", [
      '%site' => $label,
      '%login' => $url,
      '%hr' => $formatedTimeout,
    ])];

    $result = $this->mailManager->mail('one_time_login_link_admin', 'one_time_login_link', $mail, $user->getPreferredLangcode(), $params);
    if ($result['result'] != true) {
      $message = $this->t('There was a problem sending your email notification to @email.', array('@email' => $mail));
      $this->getLogger('one-time-login-link-admin')->error($message);
    }

    $this->messenger()->addMessage($this->t('One-time login link created for %mail:<br/> <code>%login</code>', [
      '%mail' => $mail,
      '%login' => $url,
    ]));

    $this->messenger()->addMessage($this->t("This link is valid for %hr, and will become invalid if the user's password is updated by anyone.", [
      '%hr' => $formatedTimeout,
    ]));

    if ($destination = $this->getRedirectDestination()->get()) {
      return new RedirectResponse($destination);
    }
  }

}
