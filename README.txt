CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

One Time Login Link Admin intends to provide an option
in the people's(User listing) page in Drupal admin from which an admin user
can generate or email a one-time login link for any user.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/one_time_login_link_admin

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/one_time_login_link_admin


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as usual, see:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Subhransu sekhar(subhransu-sekhar)- https://www.drupal.org/u/subhransu_sekhar
