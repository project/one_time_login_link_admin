<?php

/**
 * @file
 * This file is used to write the test case code in the module.
 */

namespace Drupal\Tests\one_time_login_link_admin\Unit;

use Drupal\Tests\BrowserTestBase;

use Drupal\Core\Url;

/**
 * Provide some basic test for entity create.
 */
class TestOnetimetimeloginlinkFunctional extends BrowserTestBase {

  /**
   * Our module dependencies.
   *
   * @var string[]
   */
  protected static $modules = ['one_time_login_link_admin'];

  /**
   * A user for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * A user for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
    ]);
    $this->webUser = $this->drupalCreateUser([
      'access content',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function testgenerateLoginLink() {
    // Create the user with the appropriate permission.
    // Start the session.
    $assert = $this->assertSession();
    // Login as our account.
    $this->drupalLogin($this->adminUser);
    // Check with Invalid user Id or anonymous.
    $this->drupalGet(Url::fromRoute('one_time_login_link_admin.generate_login_link',
      ['user' => 310]));
    // Check path.
    $this->assertSession()->statusCodeEquals(404);
    $assert->pageTextContains('The requested page could not be found');
  }

}
